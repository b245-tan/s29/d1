/*
MongoDB Querry Operators

Field Protection

Including or Exlucing Fields
*/

db.users.insertMany([
            {
                firstName: "Jane",
                lastName: "Doe",
                age: 21,
                contact: {
                    phone: "87654321",
                    email: "janedoe@gmail.com"
                },
                courses: [ "CSS", "Javascript", "Python" ],
                department: "HR"
            },
            {
                firstName: "Stephen",
                lastName: "Hawking",
                age: 76,
                contact: {
                    phone: "87654321",
                    email: "stephenhawking@gmail.com"
                },
                courses: [ "Python", "React", "PHP" ],
                department: "HR"
            },
            {
                firstName: "Neil",
                lastName: "Armstrong",
                age: 82,
                contact: {
                    phone: "87654321",
                    email: "neilarmstrong@gmail.com"
                },
                courses: [ "React", "Laravel", "Sass" ],
                department: "HR"
            }
        ]);


// Mini Activity

        // Re-insert the record of Bill Gates in the users database.
        /*

            firstName - "Bill"
            lastName - "Gates"
            age: 65
            contact: {
                phone - "12345678",
                email: "bill@gmail.com"
            },
            courses -  "PHP", "Laravel", "HTML"
            department: "Operations",

        */
        // Take a screenshot of result and send it to the batch hangouts.

db.users.insertOne({
	firstName: "Bill",
	lastName: "Gates",
	age: 65,
	contact: {
		phone: "12345678",
		email: "bill@gmail.com"
	},
	courses:  ["PHP", "Laravel", "HTML"],
	department: "Operations",
});

// [SECTION] Comparison Query Operators

	// @gt / @gte operator
	/*
		-Allows us to find documents that have field values greater than or equal to a specified value.

		-Syntax:
			db.collectionName.find({field: {$gt: value}});
			db.collectionName.find({field: {$gte: value}});
	*/

	db.users.find({age: {$gt: 65}}); //This will return only 2 records

	db.users.find({age: {$gte: 65}}); // This will return 3 records including the age equal to 65



	// @lt / @lte operator
	/*
		-Allows us to find documents that have field values less than or equal to a specified value.

		-Syntax:
			db.collectionName.find({field: {$lt: value}});
			db.collectionName.find({field: {$lte: value}});
	*/

	db.users.find({age: {$lt: 65}});
	db.users.find({age: {$lte: 65}});



	// $ne operator
		/*
			- Allows us to find documents that have field number values not equal to a specified value.

			- Syntax:
				db.collectionName.find({field: {$ne: value}});
		*/

	db.users.find({age: {$ne : 82}});


	// $in operator
		/*
			-Allows us to find documents with specific match criteria of one field using different values.

			- Syntax:
				db.collectionName.find({field: {$in: [valueA, valueB]}})
		*/

	db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
	db.users.find({courses: {$in: ["HTML", "React"]}});


// [SECTION]
	
	// $or operator
		/*
			-Allows us to find documents that match a single criteria from multiple provided search criteria.

			-Syntax: 
				db.collectionName.find({$or: [{fieldA:valueA}, {fieldB:valueB}]});
		*/

	db.users.find({
		$or: [
			{firstName:"Neil"},
			{age: 21}
			]
	});

	// with comparison query operator
	db.users.find({
		$or: [
			{firstName:"Jane"},
			{age: {$gt:30}}
			]
	});



	// $and operator
		/*
			- Allows us to find docuemtns matching multiple criteria in a single field.

			-Syntax:
			db.collectionName.find({$and: [{fieldA:valueA}, {fieldB:valueB}]});
		*/

	db.users.find({
		$and: [
				{age: {$ne: 82}},
				{age: {$ne: 76}}
			]
	});



// Mini Activity

        // Look for the users that have the courses "Laravel" & "React" and whose age is less than 80 years old.

        // Take a screenshot of result and send it to the batch hangouts.

        // Expected Result: Stephen Hawking and Bill Gates

	db.users.find({
		$and: [
			{courses: {$in: ["Laravel", "React"]}},
			{age: {$lt: 80}}
			]
	});


// [SECTION] Field Projection
	// To help the readability of the values returned, we can include/exclude fields from the retrieved results.

	// Inclusion
	/*
		- Allows us to include/add specific fields only when retrieveing documents.
		- The value provided is 1 to denote that the field being included. 

		- Syntax:
			db.users.find({criteria}, {field: 1})
	*/

	db.users.find(
		{
			firstName: "Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			contact: 1
		}
	);


	// Exclusion
	/*
		- Allows us to exclude/remove specific fields only when retrieving documents.
		- The value provided is 0 to denote that field is being excluded.

		-Syntax: 
			db.users.find({criteria}, {field:0});
	*/

	db.users.find(
		{
			firstName: "Jane"
		},
		{
			_id: 0,
			contact: 0,
			department: 0
		}
	);



// Mini Activity
        
        // Using the Field projection, Return the User's firstName, lastName, and contact field where lastName is equal to "Doe".

        // Take a screenshot of result and send it to the batch hangouts.

	db.users.find(
		{
			lastName: "Doe"
		},
		{
			firstName: 1,
			lastName: 1,
			contact: 1,
			_id: 0,
		}
	);

	// Supressing the ID Field
		// - When using field projection, field inclusion and exclusion may not be used at the same time.

		// - Excluding the "_id" field is the only exception to this rule.

		// Syntax: db.collectionName.find({criteria}, {field: 1, _id:0});
	
	// Return a specific field in embedded documents
	db.users.find(
			{
				firstName: "Jane"
			},
			{
				firstName: 1,
				lastName: 1,
				"contact.phone": 1
			}
		)

	// Exclude
	db.users.find(
			{
				firstName: "Jane"
			},
			{
				"contact.phone": 0
			}
		)

	// Project Specific Elements in the Returned Array
		// The $slice operator allows us to retrieve element that matches the criteria.
		/*
			- Syntax:
				db.collectionName.find({criteria}, arrayField: {$slice: count});

				db.collectionName.find({criteria}, arrayField: {$slice: [index, count]});
		*/

		// shows the elements array based on slice count.
		db.users.find(
			{
				firstName: "Jane"
			},
			{
				courses: {$slice: 2}
			}
		);


		db.users.find(
			{
				firstName: "Jane"
			},
			{
				courses: {$slice: [2, 1]}
			}
		);

// [SECTION] Evaluation Query Operator
	db.users.find({firstName: "jane"});

	// $regex operator
		/*
			- Allows us to find documents that match a specific string pattern using "regular expression" or "regex"

			- Syntax:
				db.collectionName.find({field: $regex: "pattern", $options: "optionValue"})
		*/

		// case sensitive query
		db.users.find({firstName: {$regex: "Ne"}});

		// case insensitive query
		db.users.find({firstName: {$regex: "Ne", $options: "$i"}});